# ostree-images

tl;dr: This repo contains a couple of yamls defining images for working with a [Fedora Silverblue] installation. Of course, these are suited to my package/environment needs as I don't use gnome and I'm more of a [sway] user.

This is based off: https://pagure.io/workstation-ostree-config

--

[Fedora Silverblue]: https://fedoramagazine.org/what-is-silverblue/
[sway]: https://swaywm.org/
