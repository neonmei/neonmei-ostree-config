# Changelog

## [0.1.0] - 2021-08-15

### Features

- Add base ostree configuration ([02964f5](02964f594c26672426522213f598d72a90b6fd05))
- Add fedora and rpmfusion repos ([247ffd7](247ffd7b4c4d5c091aaf4fa19f6067b2cd188570))
- Add rpmfusion gpg keys ([4d09fbb](4d09fbb3619ca7f5930012a57e4081247160017d))
- Add neonmei-workstation tree file ([69db0be](69db0befedd75995dff5c2dc05d5ebe04966ea7d))
- Add upstream postinstall script ([11ed8ac](11ed8ac6e177f44a3a26b1fdf6c78eac5c186fb3))

### Miscellaneous

- Add basic readme and gitignore ([65655e4](65655e4a49368570ab7a1664fbe21bcc4e25355b))
- Add git-cliff configuration ([2bf76e5](2bf76e59657caa9f74031b979d4413e76b10d476))

### Styling

- Add basic pre-commit configuration ([9cccb14](9cccb147db7bd2e011983e614f0c01d1a1408a78))

